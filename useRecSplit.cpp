#include <sstream>
#include <iomanip>
#include <cmath>
#include <iostream>
#include<vector>
#include <fstream>
#include <string>

using namespace std;
#include "useRecSplit.hpp"

using namespace sux::function;

template<typename ZTYPE>
string printVec(ZTYPE anum)
{
	stringstream ss;
	for (typename ZTYPE :: const_iterator d = anum.begin(); d != anum.end(); ++d)
		ss << *d << " ";
	
	string si = ss.str();
	return si;
}


double creationRecSplit55(const unsigned int * hedges, const unsigned int d, const unsigned int N, RecSplit<5, MALLOC> &rs, vector<unsigned int>& stockage)
{
	/*we are going to use 64bits as it is done in recSplit for 128bits*/

	vector<uint64_t> h64rs(N);
	uint64_t elapsed ;

	chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();
	for(unsigned int i=0; i < N; i++)
	{
		h64rs[i] = SpookyHash::Hash64((char *)&hedges[i*d], (d* sizeof(unsigned int))/sizeof(char), 0);
	}

	rs = RecSplit<5, MALLOC>(h64rs, 5);	
	for(unsigned int i=0; i < N; i++)
	{
		uint64_t mye = SpookyHash::Hash64((char *) &hedges[i*d], (d*sizeof(unsigned int))/sizeof(char), 0);
		stockage[rs(mye)] = i;
		/*calling	stockage[rs(h64rs[i])] = i; will not work, as recsplit modifies the input */
	}
	chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
	elapsed = chrono::duration_cast<chrono::nanoseconds>(t2 - t1).count();
	return (double)(elapsed * 1.E-9 );
}

void testRecSplit55( unsigned int *hedges,  unsigned int d,  unsigned int N, 
  unsigned int *queries,  unsigned int numQueries, double &cnstrTime, double &qryTime, vector<int> &resultsRecSplit)
{
	
	RecSplit<5, MALLOC> rs;
	vector<unsigned int> stockage(N);
	uint64_t elapsed ;

	cnstrTime = creationRecSplit55(hedges, d, N, rs,  stockage);
	chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();
	for (unsigned int j = 0; j < numQueries; j++)
	{
		uint64_t myq = SpookyHash::Hash64((char *) &queries[j*d], (d*sizeof(unsigned int))/sizeof(char), 0);
		unsigned int indice = stockage[rs(myq)];

		resultsRecSplit[j] = 1;
		for(unsigned int tt = 0 ; tt < d; tt++)
		{
			if (queries[j*d + tt] != hedges[indice*d + tt]) 
			{                               
				resultsRecSplit[j] = 0;
				break;
			}
		}
	}
	chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
	elapsed = chrono::duration_cast<chrono::nanoseconds>(t2 - t1).count();
	qryTime = (double)(elapsed * 1.E-9 );
}
/********************************************************************************************************************************/

double creationRecSplit(const unsigned int *hedges, const unsigned int d, const unsigned int N, RecSplit<8, MALLOC> &rs, vector<unsigned int>& stockage)
{
	/*we are going to use 64bits as it is done in recSplit for 128bits*/

	vector<uint64_t> h64rs(N);
	uint64_t elapsed ;
	chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();

	for(unsigned int i=0; i < N; i++)
	{
		h64rs[i]= SpookyHash::Hash64((char *)&hedges[i*d], (d* sizeof(unsigned int))/sizeof(char), 0);
	}

	rs = RecSplit<8, MALLOC>(h64rs, 100);	
	for(unsigned int i=0; i < N; i++)
	{
		uint64_t mye = SpookyHash::Hash64((char *) &hedges[i*d], (d*sizeof(unsigned int))/sizeof(char), 0);
		stockage[rs(mye)] = i;
		/*calling	stockage[rs(h64rs[i])] = i; will not work, as recsplit modifies the input */
	}
	chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
	elapsed = chrono::duration_cast<chrono::nanoseconds>(t2 - t1).count();
	return (double)(elapsed * 1.E-9 );
}

void testRecSplit(unsigned int *hedges, unsigned int d, unsigned int N, 
	  unsigned int *queries,  unsigned int numQueries, double &cnstrTime, double &qryTime, vector<int> &resultsRecSplit)
{
	RecSplit<8, MALLOC> rs;
	vector<unsigned int> stockage(N);
	cnstrTime = creationRecSplit(hedges, d, N, rs,  stockage);
	uint64_t elapsed ;

	chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();
	for (unsigned int j = 0; j < numQueries; j++)
	{
		uint64_t myq = SpookyHash::Hash64((char *) &queries[j*d], (d*sizeof(unsigned int))/sizeof(char), 0);

		unsigned int indice = stockage[rs(myq)];

		resultsRecSplit[j] = 1;
		for(unsigned int tt = 0 ; tt < d; tt++)
		{
			if (queries[j*d + tt] != hedges[indice*d + tt]) 
			{                               
				resultsRecSplit[j] = 0;
				break;
			}
		}
	}

	chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
	elapsed = chrono::duration_cast<chrono::nanoseconds>(t2 - t1).count();
	qryTime = (double)(elapsed * 1.E-9 );
}


