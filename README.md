
# Algorithms and data structures for hyperedge queries

This repository contains efficient algorithms
for building hash tables to answer hyperedge queries,
as discussed in the technical report:

**Jules Bertrand, Fanny Dufossé, Somesh Singh, Bora Uçar**. *Algorithms and data structures for hyperedge queries*. [Research Report] RR-9390, Inria Grenoble Rhône-Alpes. 2022 (v4).
[⟨hal-03127673⟩](https://hal.inria.fr/hal-03127673).
<pre>
@techreport{bertrand:hal-03127673,
  TITLE = {{Algorithms and data structures for hyperedge queries}},
  AUTHOR = {Bertrand, Jules and Dufoss{\'e}, Fanny and Singh, Somesh and U{\c c}ar, Bora},
  URL = {https://hal.inria.fr/hal-03127673},
  NOTE = {Revised version April 2022},
  TYPE = {Research Report},
  NUMBER = {RR-9390},
  PAGES = {28},
  INSTITUTION = {{Inria Grenoble Rh{\^o}ne-Alpes}},
  YEAR = {2022},
  MONTH = Apr,
  KEYWORDS = {Hashing ; perfect hashing ; hypergraphs ; hypergraphes ; hachage parfait ; Hachage},
  PDF = {https://hal.inria.fr/hal-03127673v4/file/RR-9390.pdf},
  HAL_ID = {hal-03127673},
  HAL_VERSION = {v4},
}
</pre>

## License
See the file LICENSE. The software is under CeCILL-B license, which is a BSD-like license.

## Contact
The e-mail addresses of the authors are:
jules.bertrand@ens-lyon.fr, fanny.dufosse@inria.fr, somesh.singh@ens-lyon.fr, and bora.ucar@ens-lyon.fr.

### Organization

The code for **FKSlean** is located in the top-level directory of the repository. 
The codes for the other hashing methods used for evaluation: [RecSplit](https://github.com/vigna/sux),
[BBHash](https://github.com/rizkg/BBHash), [PTHash](https://github.com/jermp/pthash)  and [FastFilter](https://github.com/FastFilter/fastfilter_cpp)
are located in the [*extern*](extern) folder.
The other external codes used: [fastmod](https://github.com/lemire/fastmod) and [PIGO](https://github.com/GT-TDAlab/PIGO) are also present in the [*extern*](extern) folder.

## Building and Usage

### Prerequisites
* g++ (gcc) version 9.2 or higher, with support for *pthread*
* [Boost](https://www.boost.org/) C++ libraries version 1.67.0 or higher


### Building

The codes are written in C++.

To build the code:

- Rename the file `Makefile.inc.template` to `Makefile.inc`
- In `Makefile.inc` modify the first line `CXX = g++-9` to specify the g++ (version >= 9.2) on your system.
- To build the code run `make`

The executables will be generated within the top-level directory of the repository

### Usage

* For running experiments on real-life hypergraphs:


<pre><code>
	exps_hedgeQueries
	--file <f>       : the input filename
	--numQueries <q> : the number of queries
	[--numRuns <r>]  : the number of repetitions (optional, default 5)
	[--hitRatio <t>] : the ratio of hit queries (optional: default 0.5)
	--bin &lt;b&gt;        : for reading a binary file
	--help <h>       : help
</code></pre>


Supported file formats:

- tns from [FROSTT](http://frostt.io/tensors/)
- mtx or MatrixMarket from [SuiteSparse](https://sparse.tamu.edu)

A sample file, `cage.mtx`, from SuiteSparse is provided in the repository

```
./exps_hedgeQueries -f cage3.mtx -q 1000 -m
```

The files can be converted to binary using `convertToBin` whose source code is included in
the repository. 

```
./exps_hedgeQueries -f lbnl-network.tns.bin -q 10000000 -r 5 -b -t 0.5
```

```
./exps_hedgeQueries -f kmer_A2a.mtx.bin -q 10000000 -r 5 -b -t 0.5
```

* For running experiments on synthetic, random hypergraphs:

<pre><code>
	exps_RandHedgeQueries
	--numHedges <z>  : the number of hyperedges
	--numDims <d>    : the number of dimensions
	--minSize &lt;i&gt;    : the minimum size in a dimension
	--maxSize <x>    : the maximum size in a dimension
	--numQueries <q> : the number of queries
	[--numRuns  <r>] : the number of repetitions (optional, default 5)
	[--hitRatio <t>] : the ratio of hit queries (optional: default 0.5)
	--help <h>       : help
</code></pre>



```
./exps_RandHedgeQueries -z 1000000 -d 3 -i 1000000 -x 1000001 -q 100000 -r 5 -t 0.5 
```

