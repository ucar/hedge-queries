include Makefile.inc

OBJSCOMMON =  hashalaFKS.o useRecSplit.o randUtils.o useBBH.o 

OBJS = exps_hedgeQueries.o $(OBJSCOMMON) mtxTnsIO.o  
INCS = fkslean.hpp hashalaFKS.hpp  useRecSplit.hpp randUtils.hpp useBBH.hpp usePTHash.hpp testFastFilters.hpp 

OBJS_R = exps_RandHedgeQueries.o $(OBJSCOMMON)
INCS_R = $(INCS)

TARGET = exps_hedgeQueries
all: $(TARGET) exps_RandHedgeQueries convertToBin
 
exps_hedgeQueries: $(OBJS) $(INCS)
	$(CXX) $(OBJS) $(CXXFLAGS) -march=native -pthread -I. $(SUXINCS) -o $@ 

exps_RandHedgeQueries: $(OBJS_R) $(INCS_R)
	$(CXX) $(OBJS_R) $(CXXFLAGS)  -march=native  -pthread -I. $(SUXINCS) -o $@ 

convertToBin: convertToBin.o mtxTnsIO.o mtxTnsIO.hpp 
	$(CXX) convertToBin.o mtxTnsIO.o -I. -o $@

clean:	
	$(RM) -f $(TARGET) exps_RandHedgeQueries convertToBin *.o *~

%.o : %.cpp
	$(CXX) -c $(CXXFLAGS) -I. $(SUXINCS) $(FFINCS) $(BBHINCS) -march=native   $< -o $@

hashalaFKS.o : hashalaFKS.hpp
useRecSplit.o : useRecSplit.hpp
useBBH.o : useBBH.hpp
randUtils.o : randUtils.hpp
