#include <vector>
#include <unordered_map>
#include <random>
#include <chrono> 

using namespace std;

#include "hashalaFKS.hpp"

//#define STATS

bool est_premier(unsigned i)
{
	if (i%2 == 0)
		return false;

	for (unsigned j = 3; j*j <= i; j += 2)
	{
		if (i%j == 0)
			return false;
	}
	return true;
}

void trouver_premier(const unsigned int m, unsigned int&p)
{
	unsigned i = m+1;
	while (!est_premier(i))
		i+=1;
	p=i;
}


/***************************************/

double randomnumberhala() {/*from https://stackoverflow.com/questions/56435506/rng-function-c*/
    // Making rng static ensures that it stays the same
    // Between different invocations of the function
static std::default_random_engine rng;

std::uniform_real_distribution<double> dist(0.0, 1.0); 
return dist(rng); 
}

unsigned int randomnumberinthala(unsigned int p) {/*from https://stackoverflow.com/questions/56435506/rng-function-c*/
    // Making rng static ensures that it stays the same
    // Between different invocations of the function
static std::mt19937 rng;

std::uniform_int_distribution<uint32_t> dist(0, p-1); 
return dist(rng); 
}


int test_klhala(unsigned int N, unsigned int d, unsigned int *hedges, vector<unsigned long long>& k, unsigned int pp_hala_, unsigned int qq_hala_, uint64_t M_fastmod)
{
	unsigned long long indice, somme;
	unsigned int indice2;
	vector <unsigned int> buckets(N,0);
	for (unsigned int i = 0; i < N; i++) //for each element
	{
		somme = 0;
		for (unsigned int j = 0; j < d; j++)
			somme += hedges[i*d+j] * k[j] ;
		modpMersennehala(indice, somme);

		indice2 = fastmod::fastmod_u32((unsigned int) indice, M_fastmod, N);
		buckets[indice2] ++;
	}

	somme=0;
	for (unsigned int i = 0; i < N; i++)	 //test if the bucket sizes meet the criterian with the selected k (check sum b_i^2). 
	{	
		somme += buckets[i] * buckets[i]; 
	}
	if (somme/3 < N)
	{
		return 1;
	}

	return 0;
}

void trouver_klhala(unsigned int N, unsigned int d, unsigned int * hedges, vector<unsigned long long>& k, unsigned int pp_hala, unsigned int qq_hala, uint64_t M_fastmod)
{
	for (unsigned int i = 0; i < d; i++)
	{	
		k[i] = (unsigned long long) randomnumberinthala(pp_hala);	
	}
#ifdef STATS    	
	int	 trials = 1;
#endif
	while (!test_klhala(N,d, hedges, k, pp_hala	, qq_hala, M_fastmod)) //trial and error
	{
#ifdef STATS    
		trials ++;
#endif		
		for (unsigned int i = 0; i < d; i++)
		{
			k[i] = (unsigned long long) randomnumberinthala(pp_hala);	
		}
	}
#ifdef STATS    	
	if(trials>1)
		cout << "\t\tFIND KLONG TRIALS "<< trials<<endl;
#endif	
}

void findHashalaFKSparams(unsigned int *hedges,  unsigned int d, const unsigned int * dimensions,  unsigned int N, 
	vector <unsigned long long int> &k, unsigned int &pp_hala, unsigned int &qq_hala)
{
	unsigned int larger=N;
	uint64_t M_fastmod;//, M_p_fastmod ;
	for (unsigned int i = 0; i < d; i++)
	{
		if (dimensions[i]>larger)		
			larger=dimensions[i];		
	}
/*
	if (larger<N)
		trouver_premier(N,pp_hala);
	else
		trouver_premier(larger, pp_hala);
*/
	pp_hala = 2147483647 ;/*this is 2^31-1*/
	qq_hala = 31;
	if( pp_hala < larger)
	{
		trouver_premier(larger, pp_hala);
		qq_hala = 0;

	}

	if (pp_hala == 0)
	{
		cout << "in hashalaFKS could not find a proper mersenne prime"<<endl;
		exit(12);
	}

	srand(time(NULL));
	k.resize(d);
	M_fastmod= fastmod::computeM_u32(N);

	trouver_klhala(N,d,hedges,k,pp_hala, qq_hala, M_fastmod);
}

void creation_hashalafks(unsigned int * hedges, const unsigned int N, const unsigned int d, 
	unordered_map<unsigned int*, unsigned int, hashalaFKS, hashalaFKS>& myMap)
{
/*
 * PRE: assumes the following calls and declarations are made:
 *
 *	 findHashalaFKSparams(..., d, ..., N, p, k);
 *	 unordered_map<vector<unsigned int>, unsigned int, hashalaFKS> myMap{N, hashalaFKS(d, N, p, k)};
 */
	
	for (unsigned int i=0; i < N; i++)
		myMap.insert({&(hedges[i*d]),i+1});
	
}





void testHashalaFKS(unsigned int *hedges,  unsigned int d,  unsigned int N,  const unsigned int *dimensions, unsigned int *queries,  unsigned int numQueries, double &cnstrTime, double &qryTime, vector<int> &resultsHashalaFKS)
{
	unsigned int pp_hala, qq_hala;

#ifdef STATS
	unsigned maxbucketsz;
#endif	
	vector <unsigned long long int> k;

	chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();
	findHashalaFKSparams(hedges, d, dimensions, N, k, pp_hala, qq_hala);
//	cout << " hashala: found params"<<endl;
	unordered_map<unsigned int*, unsigned int, hashalaFKS, hashalaFKS> myMap{N, hashalaFKS(d, N, k, pp_hala, qq_hala),hashalaFKS(d, N, k, pp_hala, qq_hala) };
	unordered_map<unsigned int*, unsigned int, hashalaFKS>::const_iterator got ;
	creation_hashalafks(hedges, N, d, myMap);
	chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
	
	uint64_t elapsed = chrono::duration_cast<chrono::nanoseconds>(t2 - t1).count();
	
	cnstrTime = (double)(elapsed * 1.E-9 );

#ifdef STATS
	maxbucketsz = 0;
	for (unsigned int i = 0; i < myMap.bucket_count(); i++)
	{
		if(myMap.bucket_size(i) > maxbucketsz)
			maxbucketsz = myMap.bucket_size(i);
	}
	cout <<"This is p of hashala "<< pp_hala<< " d " <<d << " LF " << myMap.load_factor()<< " bcount " <<myMap.bucket_count()<<" maxSzBckt "<< maxbucketsz<<endl;
	cout <<"This is p of hashalaLoadBucketCount "<< pp_hala<< " LF " << myMap.load_factor()<< " bcount " <<myMap.bucket_count()<<endl;
#endif

	t1 = chrono::high_resolution_clock::now();
	for (unsigned int j = 0; j < numQueries; j++)
	{
		auto range = myMap.equal_range(&(queries[j*d]));
		resultsHashalaFKS[j] = 0;
		for (auto ai = range.first; ai != range.second; ai++) {

			unsigned int index = ai->second ;
			index --;
			resultsHashalaFKS[j] = 1;
			for (unsigned int tt = 0; tt < d; tt ++)
			{
				if (queries[j*d +tt]  != hedges[index*d + tt])
				{
					resultsHashalaFKS[j] = 0;		
					break;
				}
			} 
		}
	}
	t2 = chrono::high_resolution_clock::now();
	elapsed = chrono::duration_cast<chrono::nanoseconds>(t2 - t1).count();
	qryTime = (double)(elapsed * 1.E-9 );
}

