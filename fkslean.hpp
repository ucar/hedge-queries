
#include <cstring>
#include <cmath>
#include <cassert>
#include <random>
#include <boost/dynamic_bitset.hpp>
#include <chrono>

using namespace std;
#include "extern/fastmod-master/include/fastmod.h"


//#define UNLIKELY(x) __builtin_expect((bool)(x), 0)

//#define STATS

/* computes y = x mod pp*/
#define modpMersenneExt(y, x, pp, qq)  \
{                                  \
	y = ((x) & (pp)) + ((x)>>(qq));\
	y = y >= (pp) ? y-= (pp) : y;  \
}

void findMersenneExt(unsigned int N, unsigned int &p, unsigned int &q)
{

	vector <unsigned int> qqs ({2, 3, 5, 7, 13, 17, 19, 31});
	vector <unsigned int> pps = qqs;
	unsigned int one  =1;
	p = q = 0;
	for (unsigned int i =0 ;i < pps.size(); i++)
		pps[i] = (one << qqs[i]) - 1;

	for (unsigned i = 0; i < qqs.size(); i++)
	{
		if(pps[i]>=N)
		{
			p = pps[i];
			q = qqs[i];
			break;
		}
	}
	if (p == 0)
	{
		cout << "Could not find a proper mersenne prime "<<endl;
		exit(12);
	}
}

double randomnumberExt() {/*from https://stackoverflow.com/questions/56435506/rng-function-c*/
    // Making rng static ensures that it stays the same
    // Between different invocations of the function
static std::default_random_engine rng;

std::uniform_real_distribution<double> dist(0.0, 1.0); 
return dist(rng); 
}

unsigned int randomnumberintExt(unsigned int p) {/*from https://stackoverflow.com/questions/56435506/rng-function-c*/
    // Making rng static ensures that it stays the same
    // Between different invocations of the function
static std::mt19937 rng;

std::uniform_int_distribution<uint32_t> dist(0, p-1); 
return dist(rng); 
}



int test_klExt(unsigned int N, unsigned int N1, unsigned int d, unsigned* ensemble, unsigned* items, unsigned* offset, unsigned long long * k, unsigned int p, unsigned int q, unsigned &fksStorageSize, unsigned * fksStorage_offset, unsigned* bucket_ids)
{

	unsigned long long  somme;
	unsigned int aa;

	uint64_t M_fastmod= fastmod::computeM_u32(N1); 
	//uint64_t M_p_fastmod = fastmod::computeM_u32(p);

	for (unsigned int i = 0; i < N; i++) //for each element
	{
		somme = 0;
		for (unsigned int j = 0; j < d; j++)
			somme += ensemble[i*d+j] * k[j] ;

unsigned long long indice;
		modpMersenneExt(indice, somme, p, q);
		bucket_ids[i] = fastmod::fastmod_u32((unsigned int) indice,M_fastmod, N1);
	}

	memset (offset, 0, sizeof(unsigned) * (N1+1));

	for (unsigned i = 0; i < N; i++)  
		offset[bucket_ids[i]] += 1;

	for(unsigned lo = 0; lo < N1; ++lo) 
		offset[lo+1] += offset[lo];
	

	// sorting of the non-zero item indices by bucket_id assigned to them
	// offset[x] at this point stores the starting index of the bucket (x+1)
	for(unsigned i=0; i < N; ++i) 
	{
		offset[bucket_ids[i]] -= 1; 
		items[offset[bucket_ids[i]]] = i;
	} 

	// offset[x] at this point stores the starting index of the bucket (x)
	somme=0;
	fksStorage_offset[0] = 0; 

	for (unsigned int i = 0; i < N1; i++)
	{	
		unsigned diff = offset[i+1]-offset[i];
		unsigned diff_squared = diff * diff;
		somme += diff_squared;
		if (diff > 1) fksStorage_offset[i+1] = 1 + (2 * diff_squared);
		else if (diff == 1) fksStorage_offset[i+1] = 1;
		else if (diff == 0) fksStorage_offset[i+1] = 0;
	}

    /* Populating fksStorage_offset[] with the appropriate sizes, in parallel */

	for(unsigned lo = 0; lo < N1; ++lo) 
		fksStorage_offset[lo+1] += fksStorage_offset[lo];

	fksStorageSize = fksStorage_offset[N1];
	if (somme/3 < N1)
//	if (fksStorageSize/5.99 < N1)
	{
		return 1;
	}

	return 0;
}

void trouver_klExt(unsigned int N, unsigned int N1, unsigned int d, unsigned* ensemble, unsigned* items, unsigned* offset,  unsigned long long * k, unsigned int p, unsigned int q, unsigned &fksStorageSize, unsigned * fksStorage_offset, unsigned* bucket_ids)
{
	for (unsigned int i = 0; i < d; i++)               
		k[i] = 	randomnumberintExt(p);

	int	 trials = 1;
	while (!test_klExt(N, N1, d, ensemble, items, offset, k, p, q, fksStorageSize, fksStorage_offset, bucket_ids)) 
	{
		trials ++;
		for (unsigned int i = 0; i < d; i++)	
			k[i] = randomnumberintExt(p);
	}
	if(trials>1)
		cout << "\t\tFIND KLONG TRIALS "<< trials<<endl;
}

bool test_k2lExt(unsigned int bucketiSz, unsigned int d, unsigned* ensemble, unsigned* items, unsigned* offset, unsigned bucketId, unsigned long long * k2, unsigned* fksStorage, unsigned my_fks_offset, unsigned int p, unsigned int q)
{
	unsigned sz = bucketiSz * bucketiSz;
	sz += sz; // sz --> 2 * bucketiSz * bucketiSz 

    boost::dynamic_bitset< > liste(sz); // bit vector of size 'sz' bits

    unsigned curr_bucket_offset = offset[bucketId];
    unsigned long long indice, somme;

    // initialize current bucket in fksStorage to all zeros.

    memset(&fksStorage[my_fks_offset+1], 0, sz * sizeof(unsigned) );
    //uint64_t M_p_fastmod = fastmod::computeM_u32(p);

    for (unsigned int i = 0; i < bucketiSz; i++) 
    {
    	somme=0;

    	unsigned my_index = curr_bucket_offset + i;
    	unsigned my_item = items[my_index];
    	unsigned my_item_memoize = my_item * d;

    	for (unsigned int j = 0; j < d; j++) 
    		somme +=  ensemble[my_item_memoize + j ] * k2[j] ;
    	
    	unsigned long long indice2 ;
    	modpMersenneExt(indice2, somme, p, q);
    	indice2 = indice2 % sz;

       	if (liste[indice2]) /* More than one element map to the same index in the bucket, so try out another key */
	    	return false;

    	liste[indice2] = 1;
    	fksStorage[my_fks_offset + 1 + indice2] = my_item +1;
    }
    return true;
}

bool est_premierExt(unsigned i)
{
	if (i%2 == 0)
		return false;

	for (unsigned j = 3; j*j <= i; j += 2)
	{
		if (i%j == 0)
			return false;
	}
	return true;
}

void trouver_premierExt(const unsigned int m, unsigned int&p)
{
	unsigned i = m+1;
	while (!est_premierExt(i))
		i+=1;
	p=i;
}


void creation_fksLeanExt(unsigned* hedges, const unsigned int d, const unsigned* dimensions, 
	const unsigned int N, const unsigned int N1, unsigned long long * kDict, const unsigned MAX_KEYS, unsigned ** fksStorage_formal, unsigned * fksStorage_offset, unsigned &fksStorageSize, unsigned* bucket_ids, unsigned* offset, unsigned* items, 
	unsigned int &p, unsigned int &q)
{
	unsigned int larger = N;
	if (larger < N1) larger = N1;

	for (unsigned int i = 0; i < d; i++)
	{
		if (dimensions[i] > larger)		
			larger = dimensions[i];		
	}

	p = 2147483647 ;/*this is 2^31-1*/
	q = 31;
	if( p < larger)
	{
		trouver_premierExt(larger, p);
		q = 0;
		cout << "We fixed max num elements to 2^31-1"<<endl;
		exit(12);
	}

	srand(time(NULL));

	unsigned long long k[d];
	trouver_klExt(N, N1, d, hedges, items, offset, k, p, q, fksStorageSize, fksStorage_offset, bucket_ids);

	for(unsigned dim=0; dim < d; ++dim) 
		kDict[dim] = k[dim]; // this is always the first key in kDict
	
	/* Populate kDict with (MAX_KEYS-1) keys beforehand */
	for(unsigned i = 1; i < MAX_KEYS; ++i) 
	{
		for(unsigned dim = 0; dim < d; ++dim)  	
			kDict[i*d + dim] = randomnumberintExt(p);	
	}

	unsigned * fksStorage = (unsigned*) malloc (sizeof(unsigned) * fksStorageSize);
	for (unsigned i = 0; i < N1; i++) 
	{
		unsigned my_fks_offset = fksStorage_offset[i];
		unsigned my_offset = offset[i];

		unsigned bsz = offset[i+1] - my_offset;

	    if (bsz == 1)/*one item*/
		{
			fksStorage[my_fks_offset] = items[my_offset] + 1; 
		}
		else if (bsz > 1) 
		{
			for (unsigned int k2its = 1; k2its < MAX_KEYS; k2its++)
			{
				if (test_k2lExt(bsz, d, hedges, items, offset, i, &kDict[k2its*d], fksStorage, my_fks_offset, p, q ))
				{
					fksStorage[my_fks_offset ] = k2its;
					break;
				} 
			}
           	if(fksStorage[my_fks_offset]  == 0) /* None of the existing keys worked */
			{
				cerr << "None of the keys in kDict worked.. ABORTING!"  << endl;
				fflush(stderr);
				exit(1);
			}
		}
	}

    *fksStorage_formal = fksStorage; // fksStorage will now be accessible from testFKSLean(..)
}

void analyzeFKSleanSpace(unsigned int N1, unsigned int N, unsigned int *fksStorage, unsigned int *fksStorage_offset)
{
	vector <unsigned int> histogram(15, 0);
	unsigned int maxSz= 0;
	for (unsigned int i = 0; i < N1; i++)
	{
		unsigned int sz = fksStorage_offset[i+1] - fksStorage_offset[i];
		if (sz > 1)		
			sz = (unsigned int) ceil (sqrt(sz/2));			
		
		if(sz >= histogram.size())
			histogram.resize(sz + 1, 0);
		histogram[sz] ++;
		if(sz > maxSz)
			maxSz = sz;		
	}
	cout <<"FKS-leanExt[0, 1, 2] max "<< histogram[0] / (1.0 * N1) << " " << histogram[1]/(1.0 * N) << " " << histogram[2]/(1.0 * N) << " " <<maxSz<<endl;
}

void testFKSleanExt(unsigned* hedges, const unsigned int d, const unsigned int N, const unsigned* dimensions, 
	unsigned int *queries, const unsigned int &numQueries, double &cnstrTime, double &qryTime, vector<int> &resultsFKSlean,
	double expandRatio)
{
	unsigned int p, q, N1;
	if( N < UINT_MAX/expandRatio)
		N1 = ceil(N * expandRatio);
	else 
		N1 = N;

	const unsigned MAX_KEYS = (unsigned) ceil(2*(log(N1)/log(2))); 

	unsigned long long * kDict = (unsigned long long*) malloc(sizeof(unsigned long long) * MAX_KEYS * d); 

	unsigned * fksStorage;
	unsigned * fksStorage_offset = (unsigned*) malloc (sizeof(unsigned) * (N1+1));
	unsigned fksStorageSize;
	unsigned * bucket_ids = (unsigned*) malloc (sizeof(unsigned) * N); // stores the bucket id corresponding to every non-zero. The index of the bucket_ids[.] is the index of the non-zero. So, bucket_ids[a non-zero's index] --> bucket to which the "non-zero with the supplied index" is assigned.
	/* "items" and "offset" together form the buckets... the index of the "offset" array indicates the start of a bucket.*/
	unsigned* offset = (unsigned*) malloc (sizeof(unsigned) * (N1+1)); // Count frequencies of each element using (bucket_ids[.]+1) as key. Here (N+1) is the super-set of ((max distinct element) + 1)
	unsigned * items = (unsigned*) malloc (sizeof(unsigned) * N); // stores the id's of  the non-zeros (which is between 0 and N-1) after being sorted by bucket_ids.
	uint64_t elapsed ;

	chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();

	creation_fksLeanExt(hedges,  d, dimensions, N, N1, kDict, MAX_KEYS, &fksStorage, fksStorage_offset, fksStorageSize, bucket_ids, offset, items, p,q);

	chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
	elapsed = chrono::duration_cast<chrono::nanoseconds>(t2 - t1).count();


	cnstrTime = (double)(elapsed * 1.E-9 );

//#ifdef STATS
	double rapport = (fksStorageSize+N1+1)/(1.0 * N); 
//	cout << "FKS-leanExt p is "<< p << " num different k " << MAX_KEYS << " in time " <<  cnstrTime << " second" <<endl; //On affiche la taille de la structure
	cout << "FKS-leanExt ratio to N  " << setprecision(5) << rapport << " with " << N1/(1.0*N)<< endl;
	analyzeFKSleanSpace(N1, N, fksStorage, fksStorage_offset);

//#endif
	t1 = chrono::high_resolution_clock::now();
	uint64_t M_fastmod = fastmod::computeM_u32(N1);
//	uint64_t M_p_fastmod = fastmod::computeM_u32(p);

	for (unsigned int j = 0; j < numQueries; j++)
	{
		unsigned long long somme, indice;
		unsigned int i;
		somme=0;

		for ( i = 0; i < d; i++)
			somme += kDict[i] * queries[j*d + i];


	unsigned long long b2;
		unsigned int buckno ;
		    	modpMersenneExt(b2, somme, p, q);
buckno = fastmod::fastmod_u32((unsigned int) b2,M_fastmod, N1);

		resultsFKSlean[j] = 1;

		if (fksStorage_offset[buckno+1] - fksStorage_offset[buckno] > 0) 
		{
			unsigned int sz = fksStorage_offset[buckno+1] - fksStorage_offset[buckno] ;
			unsigned long long indice2;

			if (sz == 1)
				indice = fksStorage[fksStorage_offset[buckno]]; 
			else
			{
				unsigned long long somme2 = 0;
				for ( i = 0; i < d; i++)
					somme2 += kDict[fksStorage[fksStorage_offset[buckno]]*d+i] * queries[j*d+i] ;

unsigned long long indice2 ;
    	modpMersenneExt(indice2, somme2, p, q);
    	somme2 = indice2 % (sz-1);

				indice = fksStorage[fksStorage_offset[buckno]+1+somme2];  
			}
			if(indice == 0)
			{
				resultsFKSlean[j] = 0;
			}
			else
			{
				indice --;
				for (i = 0; i < d; i++)
				{
					if (queries[j*d+i] != hedges[indice*d+i])
					{
						resultsFKSlean[j] = 0;
						break;
					}
				}	
			}
		}
		else
			resultsFKSlean[j] = 0;
	}
	t2 = chrono::high_resolution_clock::now();
	free(kDict);
	free(fksStorage);
	free(fksStorage_offset);
	free(bucket_ids);
	free(offset);
	free(items);

	elapsed = chrono::duration_cast<chrono::nanoseconds>(t2 - t1).count();
	qryTime = (double)(elapsed * 1.E-9 );
}

