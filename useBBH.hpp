#ifndef USE_BBH_HPP
#define USE_BBH_HPP

using namespace std;

#include<vector>
#include<cstdlib>
#include<iostream>

#include "extern/BBHash/BooPHF.h"

using namespace std;


typedef boomphf::SingleHashFunctor<u_int64_t>  hasher_t;
typedef boomphf::mphf<  u_int64_t, hasher_t  > boophf_t;


double creationBBH(const int gamma,   unsigned int *hedges, const unsigned int d, const unsigned int N, boophf_t *bphf, vector<unsigned int>& stockage);

void testBBH(const int gamma,  unsigned int *hedges, const unsigned int d, const unsigned int N, 
	  unsigned int *queries, const unsigned int numQueries, double &cnstrTime, double &qryTime, vector<int> &resultsBBH);

#endif
