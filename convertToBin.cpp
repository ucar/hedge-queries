#include<iostream>
#include<iomanip>
#include<string>
#include<vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <stdlib.h>
#include <ctime> 
#include <limits>
#include <unistd.h>
#include <cstdlib>
#include <getopt.h>

#include "mtxTnsIO.hpp"
//#include "radix_sort.hpp"
#include "extern/pigo/pigo.hpp"

using namespace std;


void printUsage(char *execName) 
{
	cout << "Usage : \n\t"<< execName << "\n"
	"\t--file <f>       : the file name or name in the tns case\n"
	"\t--mtx <m>        : toggle if input is MatrixMarket file\n"
	"\t--help <h>       : help\n";
}
int parseArguments(int argc, char *argv[], string &fileName, unsigned int &isMtx)
{
	fileName = "";

	isMtx = 0;
	const char* const short_opts = "f:hm";
	const option long_opts[] = {
		{"file", required_argument, nullptr, 'f'},
		{"mtx", no_argument, nullptr, 'm'},
		{"help", no_argument, nullptr, 'h'},
		{nullptr, no_argument, nullptr, 0}
	};

	while(1)
	{
		int c, option_index;
		c = getopt_long(argc, argv, short_opts,	long_opts, &option_index);
		if (c == -1)
			break;
		switch(c)
		{
			case 'f' :
			fileName = string(optarg);
			break;			
			case 'm' :
			isMtx = 1;
			break;
			case 'h' :
			case '?' :
			default:
			printUsage(argv[0]);
			return 1;
		}	
	}

	if(fileName.compare("") == 0 )
	{
		cout <<"file or numqueries or hitRatio or are not ok."<<endl;
		printUsage(argv[0]);
		return 1;
	}
	return  0;

}

unsigned int d_for_sort;
int compareduint (const void * a, const void * b)
{
	unsigned int *h1 = (unsigned int *) a;
	unsigned int *h2 = (unsigned int *) b;
	for(unsigned int d  = 0; d < d_for_sort; d++)
	{
		if( h1[d] < h2[d]) return -1;
		if( h1[d] > h2[d]) return  1;
	}
	return 0;
}


void discardDuplicateHyperedges(unsigned int *hedges, unsigned int &N, unsigned int d, unsigned int *dimensions)
{ 
	vector <unsigned int> marker(N, 1);
	unsigned int j, Norg;
	d_for_sort = d;
	qsort(hedges, N, d * sizeof(unsigned int), compareduint);
	Norg = N;
	for(unsigned int i = 1; i < N; i++)
	{
		if(compareduint(&hedges[i*d], &hedges[(i-1)*d]) == 0)
			marker[i] = 0;
	}

	j = 0;
	for (unsigned int i = 0; i < N; i++)
	{
		if(marker[i])
		{
			for (unsigned int t = 0; t < d; t++)
				hedges[j*d+t] = hedges[i*d+t];
			j++;
		}
	}
	N = j;
}
/*
void discardDuplicateHyperedges(vector <vector<unsigned int>> &hedges, unsigned int &N, unsigned int d, vector<unsigned int> &dimensions)
{
	vector <unsigned int> marker(N, 1);
	unsigned int j, Norg;

	radix_sort(hedges, dimensions, d, N);
	Norg = N;
	for(unsigned int i = 1; i < N; i++)
	{
		if(hedges[i] == hedges[i-1])
			marker[i] = 0;
	}

	j = 0;
	for (unsigned int i = 0; i < N; i++)
	{
		if(marker[i])
		{
			hedges[j] = hedges[i];
			j++;
		}
	}
	N = j;
	hedges.resize(N);
	if( N != Norg)
	{
		cout <<"There were " << Norg - N << " duplicate hyperedges. They are discarded"<<endl;
	}
}
*/
void writeTensorToPIGOfile(string outFileName, unsigned *  hedges_array, unsigned int N, unsigned int d, unsigned * dimensions_array)
{
	pigo::Tensor<uint32_t,	uint32_t,	uint32_t*,	float,	float*,	false> tns (d, N);

/*fill tns from hedges*/

	for(unsigned nnz = 0; nnz < N; ++nnz) 
	{
		for(unsigned int dim = 0; dim < d; ++dim) 		
			tns.c()[nnz*d+dim] = hedges_array[nnz*d+dim]+1 ;		
		
	}
	tns.save(outFileName);
}

int main(int argc, char* argv[])
{
	string fileName, outFileName;
	unsigned * hedges_array; 
	unsigned * dimensions_array;

	unsigned * hedges_array_read; 
	unsigned * dimensions_array_read;
	unsigned int d, N, isMtx, dread, Nread;

	if (parseArguments(argc, argv, fileName,  isMtx))
	{
		cerr<<argv[0]<<": input cannot be parsed correctly\n"<<endl;
		return 1;
	}

	if(isMtx)
		readMatrix(fileName, &hedges_array, &dimensions_array, d, N);		
	else
	  	readTensor(fileName, &hedges_array, &dimensions_array, d, N); // read in the frostt file using PIGO      

	discardDuplicateHyperedges(hedges_array, N, d, dimensions_array);

	outFileName = fileName + ".bin";
	writeTensorToPIGOfile( outFileName,  hedges_array,  N,   d,   dimensions_array);

	readTensor(outFileName, &hedges_array_read, &dimensions_array_read, dread, Nread);		

	if(Nread != N || dread != d)
	{	
		cout << N << " vs "<<Nread<<" or "<< d<< " vs "<< dread<<endl;
		exit(12);
	}

	for (unsigned int t = 0; t < d; t++)
	{
		if( dimensions_array[t] != dimensions_array_read[t])
		{
			cout << fileName<< ": " << dimensions_array[t] << " vs " <<dimensions_array_read[t]<<endl;
			//exit(12);
		}
	}
	for (unsigned int t = 0; t < N; t++)
	{
		for (unsigned int dd = 0; dd < d; dd++)
		{
			if( hedges_array[t*d+dd] != hedges_array_read[t*d+dd])
			{
				cout << fileName<< ": " <<hedges_array[t*d+dd]<< " evse " <<hedges_array_read[t*d+dd]<<endl;
				exit(12);
			}
		}
	}
	
	free(hedges_array_read);
	free(dimensions_array_read);
	free(hedges_array);
	free(dimensions_array);

	return 0;
}
