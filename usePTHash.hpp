#pragma once

#include<vector>
#include<cstdlib>
#include<iostream>

#include "extern/PTHash/pthash-master/include/pthash.hpp"

using namespace std;
using namespace pthash;

typedef single_phf<murmurhash2_64, compact_compact, true> pthash_type_CC;
typedef single_phf<murmurhash2_64, dictionary_dictionary, true> pthash_type_DD;
typedef single_phf<murmurhash2_64, elias_fano,true>  pthash_type_EF;

template <typename PTHASH_T>
double creationPTHash(const unsigned int *hedges, const unsigned int d, const unsigned int N, PTHASH_T &pthash_f, build_configuration &config, 
	vector<unsigned int>& stockage)
{
	uint64_t elapsed ;

	vector<uint64_t> input_keys(N);

	chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();
	for(unsigned int i=0; i < N; i++)
	{
		input_keys[i] = SpookyHash::Hash64((char *)&hedges[i*d], (d* sizeof(unsigned int))/sizeof(char), 0);
	}

	pthash_f.build_in_internal_memory(input_keys.begin(), input_keys.size(), config);
	stockage.resize(pthash_f.table_size());/*the non-minimal (faster) version of PTHash uses more space than n*/ 

	for(unsigned int i=0; i < N; i++)
	{
		stockage[pthash_f(input_keys[i])] = i;
	}
	chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
	elapsed = chrono::duration_cast<chrono::nanoseconds>(t2 - t1).count();
	return (double)(elapsed * 1.E-9 );
}



template <typename PTHASH_T>
double answerQueries(const unsigned int *hedges, const unsigned int d, const unsigned int N, 
	const unsigned int *queries, const unsigned int numQueries, 
	PTHASH_T &pthash_f, vector<int> &resultsPTHash,
	const vector<unsigned int>& stockage)
{
	uint64_t elapsed ;

	chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();

	for (unsigned int j = 0; j < numQueries; j++)
	{				
		uint64_t myq = SpookyHash::Hash64((char *) &queries[j*d], (d*sizeof(unsigned int))/sizeof(char), 0);
		unsigned int atValue = pthash_f(myq);
		unsigned int indice = stockage[atValue];

		resultsPTHash[j] = 1;
		if( indice >= pthash_f.table_size())
			resultsPTHash[j]  =0;
		else
		{
			for(unsigned int tt = 0 ; tt < d; tt++)
			{
				if (queries[j*d + tt] != hedges[indice*d + tt]) 
				{                               
					resultsPTHash[j] = 0;
					break;
				}
			}
		}
	}

	chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
	elapsed = chrono::duration_cast<chrono::nanoseconds>(t2 - t1).count();
	double qryTime = (double)(elapsed * 1.E-9 );

	return qryTime;
}


void testPTHash(unsigned int *hedges, const unsigned int d, const unsigned int N, 
	const unsigned int *queries, const unsigned int numQueries, double &cnstrTime, double &qryTime, vector<int> &resultsPTHash,
	int configType)
{
	vector<unsigned int> stockage(N);
	pthash_type_CC PTHash_f1;
	pthash_type_DD PTHash_f2;
	pthash_type_EF PTHash_f3;
	pthash_type_CC PTHash_f4;

	build_configuration config;
	config.minimal_output = true;  /*(it seems that this must be equivalent to the third templated argument BU */
	config.verbose_output = false;
	switch(configType)
	{
		case 1:
		config.c = 7.0;
		config.alpha = 0.99;
		config.verbose_output = false;

		cnstrTime = creationPTHash(hedges, d, N, PTHash_f1, config, stockage);
		qryTime = answerQueries(hedges,  d, N, queries, numQueries, PTHash_f1, resultsPTHash,	stockage);
		break;

		case 2:
		config.c = 11.0;
		config.alpha = 0.88;
		config.verbose_output = false;

		cnstrTime = creationPTHash(hedges, d, N, PTHash_f2, config, stockage);
		qryTime = answerQueries(hedges,  d, N, queries, numQueries, PTHash_f2, resultsPTHash, stockage);
		break;

		case 3:
		config.c = 6.0;
		config.alpha = 0.99;
		config.verbose_output = false;

		cnstrTime = creationPTHash(hedges, d, N, PTHash_f3, config, stockage);
		qryTime = answerQueries(hedges,  d, N, queries, numQueries, PTHash_f3, resultsPTHash, stockage);
		break;

		case 4:
		config.c = 7.0;
		config.alpha = 0.94;
		config.verbose_output = false;

		cnstrTime = creationPTHash(hedges, d, N, PTHash_f4, config, stockage);
		qryTime = answerQueries(hedges,  d, N, queries, numQueries, PTHash_f4, resultsPTHash, stockage);
		break;

		default: 
		cout<<"not a correct option for using PTHash"<<endl;
		exit(12);
	}
}
