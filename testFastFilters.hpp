#include <sstream> 
#include <climits>
#include "extern/fastfilter_cpp/src/xorfilter/3wise_xor_binary_fuse_filter_naive.h" 
#include "hashalaFKS.hpp"
using namespace xorbinaryfusefilter_naive;

double creationFastFilter(const unsigned int *hedges, const unsigned int d, const unsigned int N, XorBinaryFuseFilter<uint64_t, uint16_t, SimpleMixSplit> &xbf3wise)
{
    uint64_t elapsed ;

    uint64_t  *input_keys = new uint64_t[N];

    chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();
    for(unsigned int i=0; i < N; i++)
    {
        input_keys[i] = SpookyHash::Hash64((char *)&hedges[i*d], (d* sizeof(unsigned int))/sizeof(char), 0);
    }

    xbf3wise.AddAll(input_keys, 0, N);
    chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
    elapsed = chrono::duration_cast<chrono::nanoseconds>(t2 - t1).count();
    
    delete input_keys;

    return (double)(elapsed * 1.E-9 );
}


double answerQueries(const unsigned int *hedges, const unsigned int d, const unsigned int N, 
    const unsigned int *queries, const unsigned int numQueries, 
    XorBinaryFuseFilter<uint64_t, uint16_t, SimpleMixSplit> &xbf3wise, vector<int> &resultsFilter)
{
    string s;
    uint64_t elapsed ;

    chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();

    for (unsigned int j = 0; j < numQueries; j++)
    {               
        uint64_t myq = SpookyHash::Hash64((char *) &queries[j*d], (d*sizeof(unsigned int))/sizeof(char), 0);
        Status ffResult = xbf3wise.Contain(myq);
        if(ffResult == Ok)
            resultsFilter[j] = 1;
        else if(ffResult == NotFound)
        {
            resultsFilter[j] = 0;
        }
        else
        {
            cout <<"An error occured for fastfilter"<<endl;
            exit(14);
        }
    }

    chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
    elapsed = chrono::duration_cast<chrono::nanoseconds>(t2 - t1).count();
    double qryTime = (double)(elapsed * 1.E-9 );

    return qryTime;
}


enum algForPoss { useFksLean4pos = 1, usePtHash4pos = 2, useHashala4pos = 3 };

void testFilter(unsigned int *hedges, const unsigned int d, const unsigned int N, const unsigned int *dimensions,
    const unsigned int *queries, const unsigned int numQueries, double &cnstrTime, double &qryTime, vector<int> &resultsFilter,
    algForPoss algo )
{
    XorBinaryFuseFilter<uint64_t, uint16_t, SimpleMixSplit> xbf3wise(N);
    cnstrTime = creationFastFilter(hedges, d,  N, xbf3wise);
    qryTime = answerQueries(hedges , d, N, queries, numQueries, xbf3wise, resultsFilter);

    /*for each positive answer, test in some other method*/
    vector <unsigned int> posQIndices;

    posQIndices.reserve(numQueries);/*we allocate large enough space*/

    for (unsigned int i = 0; i < numQueries; i++)
    {
        if(resultsFilter[i] == 1)
        {
            posQIndices.push_back(i);   
        }
    }
     unsigned int *posQueries  = NULL;
    if (posQIndices.size())
        posQueries =  new unsigned int[posQIndices.size() * d];
    for (unsigned int i = 0; i < posQIndices.size(); i++)
    {
        unsigned int qno = posQIndices[i];
        for (unsigned int tt = 0; tt < d; tt++)
        {
            posQueries[i * d + tt] = queries[qno * d + tt];
        }
    }
    if(posQIndices.size() > 0)
    {
        vector <int> posResultsFilter (posQIndices.size());
        uint64_t elapsed ;

        double posCnstrTime, posQryTime;
        switch (algo)
        {
            case usePtHash4pos:
                testPTHash(hedges, d,  N, posQueries, posQIndices.size(), posCnstrTime, posQryTime, posResultsFilter, 1);
                break;
            case useFksLean4pos: /*TODO: maybe use Ext here*/
                testFKSleanExt(hedges, d,  N, dimensions, posQueries, posQIndices.size(), posCnstrTime, posQryTime, posResultsFilter, 2.4);
                break;
            case useHashala4pos:
                testHashalaFKS(hedges, d, N, dimensions, posQueries, posQIndices.size(), posCnstrTime, posQryTime, posResultsFilter);
                break;
            default:
                cout <<"not a suitable choise for checkign the positives of fastfilters"<<endl;
                exit(14);
        }

        chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();
        for (unsigned int i = 0; i < posQIndices.size(); i++)
        {
            unsigned int qno = posQIndices[i];
            resultsFilter[qno] = posResultsFilter[i];
        }
        chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
        elapsed = chrono::duration_cast<chrono::nanoseconds>(t2 - t1).count();
        cnstrTime += posCnstrTime ;
        qryTime = qryTime + posQryTime + elapsed * 1.E-9 ;
    }
    if (posQIndices.size())
        delete posQueries;
}



