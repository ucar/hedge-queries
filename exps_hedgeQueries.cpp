#include<iostream>
#include<iomanip>
#include<string>
#include<vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <stdlib.h>
#include <ctime> 
#include <limits>
#include <unistd.h>
#include <cstdlib>
#include <getopt.h>

#include "fkslean.hpp"

#include "hashalaFKS.hpp"
#include "useRecSplit.hpp"
#include "randUtils.hpp"
#include "mtxTnsIO.hpp"
#include "useBBH.hpp"
#include "usePTHash.hpp"

#include "testFastFilters.hpp"
using namespace std;

//#define STATS



void printUsage(char *execName) 
{
	cout << "Usage : \n\t"<< execName << "\n"
	"\t--file <f>       : the file name or name in the tns case\n"
	"\t--numQueries <q> : the nulmber of queries\n"
	"\t[--numRuns <r>]  : the number of repetitions (optional, default 5)\n"
	"\t[--hitRatio <t>] : the ratio of hit queries (optional: defailt 0.5)\n"
	"\t--bin <b>        : toogle if binary file ready. The flag mtx is ignored if b present.\n"
	"\t--mtx <m>        : toggle if input is MatrixMarket file\n"
	"\t--help <h>       : help\n";
}
int parseArguments(int argc, char *argv[], string &fileName, unsigned int &numQueries, unsigned int &numTests, unsigned int &isMtx,  double &hitRatio, unsigned int &isBinary)
{
	fileName = "";

	numTests = 5;
	isMtx = 0;
	isBinary = 0;
	numQueries = 0;

	const char* const short_opts = "f:q:r:t:hmb";
	const option long_opts[] = {
		{"file", required_argument, nullptr, 'f'},
		{"numQueries", required_argument, nullptr, 'q'},
		{"numRuns", optional_argument, nullptr, 'r'},
		{"hitRatio", optional_argument, nullptr, 't'},
		{"bin", no_argument, nullptr, 'b'},
		{"mtx", no_argument, nullptr, 'm'},
		{"help", no_argument, nullptr, 'h'},
		{nullptr, no_argument, nullptr, 0}
	};

	while(1)
	{
		int c, option_index;
		c = getopt_long(argc, argv, short_opts,	long_opts, &option_index);
		if (c == -1)
			break;
		switch(c)
		{
			case 'f' :
			fileName = string(optarg);
			break;
			case 'q' :			
			numQueries = stoi(optarg);
			break;
			case 'r' :
			numTests = stoi(optarg);
			break;
			case 't' :
			hitRatio = (double) stof(optarg);
			break;
			case 'm' :
			isMtx = 1;
			break;
			case 'b' :
			isBinary = 1;
			break;
			case 'h' :
			case '?' :
			default:
			printUsage(argv[0]);
			return 1;
		}	
	}

	if(fileName.compare("") == 0 || numQueries == 0 || hitRatio < 0 || hitRatio > 1.0  || numTests < 1)
	{
		cout <<"file or numqueries or hitRatio or are not ok."<<endl;
		printUsage(argv[0]);
		return 1;
	}
	return  0;

}


int main(int argc, char* argv[])
{
	string fileName ;
	unsigned int numQueries;



	unsigned int numTests ;	
	unsigned int d, N, isMtx, isBinary;
	string msg, dimMsg;
	double hitRatio ;

	if (parseArguments(argc, argv, fileName, numQueries, numTests, isMtx, hitRatio, isBinary))
	{
		cerr<<argv[0]<<": input cannot be parsed correctly\n"<<endl;
		return 1;
	}

	unsigned * hedges_array; 
	unsigned * dimensions_array;
	if(isBinary)
	{
//		string binFileName = fileName + ".bin";
        	readTensor(fileName, &hedges_array, &dimensions_array, d, N); // read in the frostt file using PIGO      
//		cout <<"readbinaryfile "<< binFileName<<endl;
	}
	else
	{	
		if(isMtx)		
			readMatrix(fileName, &hedges_array, &dimensions_array, d, N);		
		else		
        	readTensor(fileName, &hedges_array, &dimensions_array, d, N); // read in the frostt file using PIGO      
   }

	unsigned int * queries_array = (unsigned int *) malloc (sizeof(unsigned int) * numQueries * d); // rows = N; cols = d;
	createQueriesMix(hedges_array, dimensions_array,  N, d, numQueries, hitRatio, queries_array);



	//vector<int> resultsRdx(numQueries);

	vector<int> resultsFKSlean(numQueries);
        
	vector<int> resultsHashalaFKS(numQueries);
	vector<int> resultsRecSplit(numQueries);
	vector<int> resultsRecSplit55(numQueries);

	vector<int> resultsBBH1(numQueries);
	vector<int> resultsBBH5(numQueries);

	vector<int> resultsPTHash1(numQueries);
	vector<int> resultsPTHash2(numQueries);
	vector<int> resultsPTHash3(numQueries);
	vector<int> resultsPTHash4(numQueries);

	vector<int> resultsFilter(numQueries);

	double execTimesFKSlean[numTests][2] = {0,0};
	double execTimesFKSleanExt125[numTests][2] = {0,0};
	double execTimesFKSleanExt150[numTests][2] = {0,0};
	double execTimesFKSleanExt175[numTests][2] = {0,0};
	double execTimesFKSleanExt200[numTests][2] = {0,0};
	double execTimesFKSleanExt225[numTests][2] = {0,0};
	double execTimesFKSleanExt240[numTests][2] = {0,0};
	double execTimesFKSleanExt250[numTests][2] = {0,0};
	double execTimesFKSleanExt275[numTests][2] = {0,0};
	double execTimesFKSleanExt300[numTests][2] = {0,0};
	double execTimesFKSleanExt325[numTests][2] = {0,0};
	double execTimesFKSleanExt350[numTests][2] = {0,0};
	double execTimesFKSleanExt375[numTests][2] = {0,0};
	double execTimesFKSleanExt400[numTests][2] = {0,0};

	double execTimesHashalaFKS[numTests][2] = {0,0};
	double execTimesRecSplit[numTests][2] = {0,0};
	double execTimesRecSplit55[numTests][2] = {0,0};

	double execTimesBBH1[numTests][2] = {0,0};
	double execTimesBBH5[numTests][2] = {0,0};

	double execTimesPTHash1[numTests][2] = {0,0};
	double execTimesPTHash2[numTests][2] = {0,0};
	double execTimesPTHash3[numTests][2] = {0,0};
	double execTimesPTHash4[numTests][2] = {0,0};

	double execTimesFilter[numTests][2] = {0,0};

/****/

	for (unsigned int i = 0; i<numTests ;i++)
	{		 
		cout << "Will run testFilter"<<endl;					

//testFilter(hedges_array,  d,  N,   dimensions_array,  queries_array,  numQueries, execTimesFilter[i][0], execTimesFilter[i][1], resultsFilter, usePtHash4pos);
		testFilter(hedges_array,  d,  N,   dimensions_array,  queries_array,  numQueries, execTimesFilter[i][0], execTimesFilter[i][1], resultsFilter, useFksLean4pos);
//testFilter(hedges_array,  d,  N,   dimensions_array,  queries_array,  numQueries, execTimesFilter[i][0], execTimesFilter[i][1], resultsFilter, useHashala4pos);
/*
		randPermuteHedges(hedges, N, d);//because tesetRadix sorts...
			cout << "Will run radix"<<endl;
			testRadix(hedges, d, N, dimensions, queries, numQueries, execTimesRadix[i][0], execTimesRadix[i][1], resultsRdx);
*/
		cout << "Will run FKSLean"<<endl;					
		testFKSleanExt(hedges_array, d,  N, dimensions_array, queries_array, numQueries, execTimesFKSlean[i][0], execTimesFKSlean[i][1], resultsFKSlean, 2.40);

		cout << "Will run hashala"<<endl;
		testHashalaFKS(hedges_array, d, N, dimensions_array, queries_array, numQueries, execTimesHashalaFKS[i][0], execTimesHashalaFKS[i][1], resultsHashalaFKS);
		cout << "Will run uRecSplit"<<endl;			
		testRecSplit(hedges_array, d,  N, queries_array, numQueries, execTimesRecSplit[i][0], execTimesRecSplit[i][1], resultsRecSplit);

		cout << "Will run uRecSplit-55"<<endl;			
		testRecSplit55(hedges_array, d,  N, queries_array, numQueries, execTimesRecSplit55[i][0], execTimesRecSplit55[i][1], resultsRecSplit55);

		cout << "Will run uBBH1"<<endl;			
		testBBH(1, hedges_array, d,  N, queries_array, numQueries, execTimesBBH1[i][0], execTimesBBH1[i][1], resultsBBH1);

		cout << "Will run uBBH5"<<endl;			
		testBBH(5, hedges_array, d,  N, queries_array, numQueries, execTimesBBH5[i][0], execTimesBBH5[i][1], resultsBBH5);

		cout << "Will run uPTHash1"<<endl;
		testPTHash(hedges_array,  d, N,  queries_array, numQueries, execTimesPTHash1[i][0], execTimesPTHash1[i][1], resultsPTHash1, 1);

		cout << "Will run uPTHash2"<<endl;
		testPTHash(hedges_array,  d, N, queries_array, numQueries, execTimesPTHash2[i][0], execTimesPTHash2[i][1], resultsPTHash2, 2);

		cout << "Will run uPTHash3"<<endl;
		testPTHash(hedges_array,  d, N, queries_array, numQueries, execTimesPTHash3[i][0], execTimesPTHash3[i][1], resultsPTHash3, 3);

		cout << "Will run uPTHash4"<<endl;
		testPTHash(hedges_array,  d, N, queries_array, numQueries, execTimesPTHash4[i][0], execTimesPTHash4[i][1], resultsPTHash4, 4);
/**/
		for (unsigned int jj = 0; jj < numQueries; jj++)
		{
/*
				if(resultsFKSlean[jj] != resultsRdx[jj])
				{
					cerr << resultsFKSlean[jj] << " vs " << resultsRdx[jj]<< " at " << jj<< " for FKS vs radix "<< endl;
					exit(23);
				}
				
			
*/
			
			if(resultsFKSlean[jj] != resultsFilter[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsFilter[jj]<< " at " << jj<< " for FKS vs resultsFilter "<< endl;
				exit(25);
			}


			if(resultsFKSlean[jj] != resultsHashalaFKS[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsHashalaFKS[jj]<< " at " << jj<< " for FKS vs hashalaFKS "<< endl;
				exit(25);
			}
	
			if(resultsFKSlean[jj] != resultsRecSplit[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsRecSplit[jj]<< " at " << jj<< " for FKS vs RecSplit "<< endl;
				exit(25);
			}

			
			if(resultsFKSlean[jj] != resultsRecSplit55[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsRecSplit55[jj]<< " at " << jj<< " for FKS vs RecSplit55 "<< endl;
				exit(25);
			}

			if(resultsFKSlean[jj] != resultsBBH1[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsBBH1[jj]<< " at " << jj<< " for FKS vs BBH1 "<< endl;
				exit(25);
			}
			if(resultsFKSlean[jj] != resultsBBH5[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsBBH5[jj]<< " at " << jj<< " for FKS vs BBH5 "<< endl;
				exit(25);
			}
	        

			if(resultsFKSlean[jj] != resultsPTHash1[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsPTHash1[jj]<< " at " << jj<< " for FKS vs PTHash1 "<< endl;
				exit(25);
			}

			if(resultsFKSlean[jj] != resultsPTHash2[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsPTHash2[jj]<< " at " << jj<< " for FKS vs PTHash2 "<< endl;
				exit(25);
			}

			if(resultsFKSlean[jj] != resultsPTHash3[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsPTHash3[jj]<< " at " << jj<< " for FKS vs PTHash3 "<< endl;
				exit(25);
			}

			if(resultsFKSlean[jj] != resultsPTHash4[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsPTHash4[jj]<< " at " << jj<< " for FKS vs PTHash4 "<< endl;
				exit(25);
			}			
		}
	}
	dimMsg = to_string(d) + " ";
	for (unsigned int i = 0; i < d ; i++)
		dimMsg  += to_string(dimensions_array[i]) + " " ;
/*
		msg = dimMsg + " "+ to_string(N) +  "  RadixSortTimings ";
		reportTime(execTimesRadix, numTests, msg);
*/
	msg = dimMsg + " "+ to_string(N) +  "    FKSleanTimings ";
	reportTime(execTimesFKSlean, numTests, msg);

	msg = dimMsg + " "+ to_string(N) +  "   FastFilters ";
	reportTime(execTimesFilter, numTests, msg);

	msg = dimMsg + " "+ to_string(N) +  " HashalaFKSTimings ";
	reportTime(execTimesHashalaFKS, numTests, msg);

	msg = dimMsg + " "+ to_string(N) +  "   RecSplitTimings ";
	reportTime(execTimesRecSplit, numTests, msg);


	msg = dimMsg + " "+ to_string(N) +  "   RecSplitTimings55 ";
	reportTime(execTimesRecSplit55, numTests, msg);



	msg = dimMsg + " "+ to_string(N) +  "   BBH1 ";
	reportTime(execTimesBBH1, numTests, msg);

	msg = dimMsg + " "+ to_string(N) +  "   BBH5 ";
	reportTime(execTimesBBH5, numTests, msg);

	msg = dimMsg + " "+ to_string(N) +  "   PTHash1 ";
	reportTime(execTimesPTHash1, numTests, msg);

	msg = dimMsg + " "+ to_string(N) +  "   PTHash2 ";
	reportTime(execTimesPTHash2, numTests, msg);

	msg = dimMsg + " "+ to_string(N) +  "   PTHash3 ";
	reportTime(execTimesPTHash3, numTests, msg);

	msg = dimMsg + " "+ to_string(N) +  "   PTHash4 ";
	reportTime(execTimesPTHash4, numTests, msg);

	free(queries_array);
	free(dimensions_array);
	free(hedges_array);

	return 0;
}
