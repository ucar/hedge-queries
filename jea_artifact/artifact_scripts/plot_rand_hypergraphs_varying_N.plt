set encoding utf8
set term pdf enhanced
set tics scale 1.5  # specify the size of the tics
set tics font ",14"
#set key default
set key top left
set key at screen 0.08, 0.97 font ",11.75" vertical sample 5 spacing 1.1 width 1 height 1.4 maxrows 4

set logscale x; 

set bmargin 4 # set the margin from the bottom
set xrange [ 800000 : 650000000]
set xtics ("10^6" 1000000, "5x10^6" 5000000, "10^7" 10000000, "5x10^7" 50000000,"10^8" 100000000, "5x10^8" 500000000)nomirror
unset mxtics
set tics out

############# plot for construction time ############
set output "../outputs_data/Figure4.pdf"
set xlabel "{/Helvetica-Italic n}" font ",19.5";
set ylabel "Construction Time [s]" font ",19.5";
set ylabel offset -0.5,0
set yrange [*:75000]
set logscale y 10
set format y "10^{%T}"
set ytics nomirror
unset mytics
plot \
  '../outputs_data/construction-time-rand-hypergraph-t0.5.dat' using 1:2 title "uRecSplit(8,100)" linetype 5 lc rgb 'black' lw 1.75 ps 0.75 pt 4 with linespoints, \
  '' using 1:5 title "uPTHash(1)" linetype 1 lc rgb '#9400D3' lw 1.75 ps 0.75 pt 11 with linespoints, \
  '' using 1:8 title "uBBH(5)" linetype 7 dt 2 lc rgb 'blue' lw 1.75 ps 0.75 pt 7 with linespoints, \
  '' using 1:10 title "HashàlaFKS" linetype 3 lc rgb '#008040' lw 1.75 ps 0.75 pt 3 with linespoints, \
  '' using 1:3 title "uBBH(1)" linetype 7 dt 2 lc rgb 'blue' lw 1.75 ps 0.75 pt 2 with linespoints, \
  '' using 1:6 title "uPTHash(4)" linetype 1 lc rgb 'red' lw 1.75 ps 0.75 pt 1 with linespoints, \
  '' using 1:9 title "uPTHash(2)" linetype 9 dt 2 lc rgb '#9400D3' lw 1.75 ps 0.75 pt 9 with linespoints, \
  '' using 1:11 title "FKSlean" linetype 8 dt 2 lc rgb 'brown' lw 1.75 ps 0.75 pt 8 with linespoints, \
  '' using 1:4 title "uPTHash(3)" linetype 9 dt 2 lc rgb 'red' lw 1.75 ps 0.75 pt 6 with linespoints, \
  '' using 1:7 title "uRecSplit(5,5)" linetype 5 lc rgb 'black' lw 1.75 ps 0.75 pt 5 with linespoints, \

############# plot for query time ############
unset logscale y;
unset format y
set ylabel offset 0,0
set output "../outputs_data/Figure5.pdf"
set xlabel "{/Helvetica-Italic n}" font ",19.5";
set ylabel "Query Response Time [s]" font ",19.5";
set yrange [0:11]
#set yrange [0:*]
set ytics 0,1,8 nomirror
plot \
  '../outputs_data/query-time-rand-hypergraph-q10million-t0.5.dat' using 1:2 title "HashàlaFKS" linetype 3 lc rgb '#008040' lw 1.75 ps 0.75 pt 3 with linespoints, \
  '' using 1:5 title "uRecSplit(8,100)" linetype 5 lc rgb 'black' lw 1.75 ps 0.75 pt 4 with linespoints, \
  '' using 1:8 title "uPTHash(3)" linetype 9 dt 2 lc rgb 'red' lw 1.75 ps 0.75 pt 6 with linespoints, \
  '' using 1:10 title "uPTHash(1)" linetype 1 lc rgb '#9400D3' lw 1.75 ps 0.75 pt 11 with linespoints, \
  '' using 1:3 title "uRecSplit(5,5)" linetype 5 lc rgb 'black' lw 1.75 ps 0.75 pt 5 with linespoints, \
  '' using 1:6 title "uBBH(5)" linetype 7 dt 2 lc rgb 'blue' lw 1.75 ps 0.75 pt 7 with linespoints, \
  '' using 1:9 title "uPTHash(4)" linetype 1 lc rgb 'red' lw 1.75 ps 0.75 pt 1 with linespoints, \
  '' using 1:11 title "FKSlean" linetype 8 dt 2 lc rgb 'brown' lw 1.75 ps 0.75 pt 8 with linespoints, \
  '' using 1:4 title "uBBH(1)" linetype 7 dt 2 lc rgb 'blue' lw 1.75 ps 0.75 pt 2 with linespoints, \
  '' using 1:7 title "uPTHash(2)" linetype 9 dt 2 lc rgb '#9400D3' lw 1.75 ps 0.75 pt 9 with linespoints, \


