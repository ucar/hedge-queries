#!/bin/bash

# for plots in figures 4 and 5

    echo "N RecSplitTimings BBH1 PTHash3 PTHash1 PTHash4 RecSplitTimings55 BBH5 PTHash2 HashalaFKSTimings FKSleanTimings" > ../outputs_data/construction-time-rand-hypergraph-t0.5.dat

    echo "N HashalaFKSTimings RecSplitTimings55 BBH1 RecSplitTimings BBH5 PTHash2 PTHash3 PTHash4 PTHash1 FKSleanTimings" > ../outputs_data/query-time-rand-hypergraph-q10million-t0.5.dat

for n in 1000000 5000000 10000000 50000000 100000000 500000000
do
    OutFile="../outputs_data/random_n_"$n"_d_4_t_0.5.out"
    ../exps_RandHedgeQueries -z $n -d 4 -i 1000000 -x 1000001 -q 10000000 -r 5 -t 0.5 > $OutFile

    dim=`tail -1 $OutFile | cut -d ' ' -f 1`
    avgConstructionTimeOffset=$((dim + 6))
    avgConstructionTime_Hashala=`tail -11 $OutFile | head -3 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_RecSplit=`tail -11 $OutFile | head -2 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_RecSplit55=`tail -11 $OutFile | head -4 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_BBH1=`tail -11 $OutFile | head -6 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_BBH5=`tail -11 $OutFile | head -7 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_PTHash1=`tail -11 $OutFile | head -8 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_PTHash2=`tail -11 $OutFile | head -9 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_PTHash3=`tail -11 $OutFile | head -10 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_PTHash4=`tail -1 $OutFile | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_FKSlean=`tail -11 $OutFile | head -1 | cut -d ' ' -f $avgConstructionTimeOffset`


    avgQueryTimeOffset=$((dim + 10))

    avgQueryTime_Hashala=`tail -11 $OutFile | head -3 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_RecSplit=`tail -11 $OutFile | head -2 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_RecSplit55=`tail -11 $OutFile | head -4 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_BBH1=`tail -11 $OutFile | head -6 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_BBH5=`tail -11 $OutFile | head -7 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_PTHash1=`tail -11 $OutFile | head -8 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_PTHash2=`tail -11 $OutFile | head -9 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_PTHash3=`tail -11 $OutFile | head -10 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_PTHash4=`tail -1 $OutFile | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_FKSlean=`tail -11 $OutFile | head -1 | cut -d ' ' -f $avgQueryTimeOffset`


    echo $n $avgConstructionTime_RecSplit $avgConstructionTime_BBH1 $avgConstructionTime_PTHash3 $avgConstructionTime_PTHash1 $avgConstructionTime_PTHash4 $avgConstructionTime_RecSplit55 $avgConstructionTime_BBH5 $avgConstructionTime_PTHash2 $avgConstructionTime_Hashala $avgConstructionTime_FKSlean >> ../outputs_data/construction-time-rand-hypergraph-t0.5.dat

    echo $n $avgQueryTime_Hashala $avgQueryTime_RecSplit55 $avgQueryTime_BBH1 $avgQueryTime_RecSplit $avgQueryTime_BBH5 $avgQueryTime_PTHash2 $avgQueryTime_PTHash3 $avgQueryTime_PTHash4 $avgQueryTime_PTHash1 $avgConstructionTime_FKSlean >> ../outputs_data/query-time-rand-hypergraph-q10million-t0.5.dat
done

   gnuplot plot_rand_hypergraphs_varying_N.plt

    ### figure 6 ##########

    echo "d PTHash4 RecSplitTimings55 HashalaFKSTimings PTHash2 BBH5 FKSleanTimings" > ../outputs_data/construction-time-rand-hypergraph-n20million-t0.5.dat

    echo "d HashalaFKSTimings RecSplitTimings55 BBH5 PTHash2 PTHash4 FKSleanTimings" > ../outputs_data/query-time-rand-hypergraph-n20million-t0.5.dat


    for dim in 4 8 16
    do
    OutFile1="../outputs_data/random_n_20million_d_"$dim"_t_0.5.out"

    ../exps_RandHedgeQueries -z 20000000 -d $dim -i 1000000 -x 1000001 -q 10000000 -r 5 -t 0.5 > $OutFile1

    dim=`tail -1 $OutFile1 | cut -d ' ' -f 1`
    avgConstructionTimeOffset=$((dim + 6))
    avgConstructionTime_Hashala=`tail -11 $OutFile1 | head -3 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_RecSplit=`tail -11 $OutFile1 | head -2 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_RecSplit55=`tail -11 $OutFile1 | head -4 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_BBH1=`tail -11 $OutFile1 | head -6 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_BBH5=`tail -11 $OutFile1 | head -7 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_PTHash1=`tail -11 $OutFile1 | head -8 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_PTHash2=`tail -11 $OutFile1 | head -9 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_PTHash3=`tail -11 $OutFile1 | head -10 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_PTHash4=`tail -1 $OutFile1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_FKSlean=`tail -11 $OutFile1 | head -1 | cut -d ' ' -f $avgConstructionTimeOffset`


    avgQueryTimeOffset=$((dim + 10))

    avgQueryTime_Hashala=`tail -11 $OutFile1 | head -3 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_RecSplit=`tail -11 $OutFile1 | head -2 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_RecSplit55=`tail -11 $OutFile1 | head -4 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_BBH1=`tail -11 $OutFile1 | head -6 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_BBH5=`tail -11 $OutFile1 | head -7 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_PTHash1=`tail -11 $OutFile1 | head -8 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_PTHash2=`tail -11 $OutFile1 | head -9 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_PTHash3=`tail -11 $OutFile1 | head -10 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_PTHash4=`tail -1 $OutFile1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_FKSlean=`tail -11 $OutFile1 | head -1 | cut -d ' ' -f $avgQueryTimeOffset`

     echo $dim $avgConstructionTime_PTHash4 $avgConstructionTime_RecSplit55 $avgConstructionTime_Hashala $avgConstructionTime_PTHash2 $avgConstructionTime_BBH5 $avgConstructionTime_FKSlean >> ../outputs_data/construction-time-rand-hypergraph-n20million-t0.5.dat

     echo $dim $avgQueryTime_Hashala $avgQueryTime_RecSplit55 $avgQueryTime_BBH5 $avgQueryTime_PTHash2 $avgQueryTime_PTHash4 $avgQueryTime_FKSlean >> ../outputs_data/query-time-rand-hypergraph-n20million-t0.5.dat

   done

   gnuplot plot_rand_hypergraphs.plt
