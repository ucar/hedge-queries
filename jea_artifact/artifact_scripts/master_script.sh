#/bin/bash

#cd ..
#cp ../*.hpp .
#cp ../*.cpp .
#cp -r ../extern .
#make clean
#make
#mkdir -p ./outputs_data
#cd -

FILES="
nell-1
delicious-3d
delicious-4d
flickr-3d
flickr-4d
nell-2
enron
vast-2015-mc1-3d
vast-2015-mc1-5d
chicago-crime-comm
uber
lbnl-network
"

echo "***************************************************************************************"
echo "*"
echo "* uncomment the following lines to download tensors from the frostt collection" 
echo "* and matrices from the SuiteSparse collection. And remove the follwing exit statement."
echo "*"
echo "***************************************************************************************"

exit

# fetch the input files

# wget -c https://s3.us-east-2.amazonaws.com/frostt/frostt_data/lbnl-network/lbnl-network.tns.gz
# wget -c https://s3.us-east-2.amazonaws.com/frostt/frostt_data/uber-pickups/uber.tns.gz
# wget -c https://s3.us-east-2.amazonaws.com/frostt/frostt_data/chicago-crime/comm/chicago-crime-comm.tns.gz
# wget -c https://s3.us-east-2.amazonaws.com/frostt/frostt_data/vast-2015-mc1/vast-2015-mc1-5d.tns.gz
# wget -c https://s3.us-east-2.amazonaws.com/frostt/frostt_data/vast-2015-mc1/vast-2015-mc1-3d.tns.gz
# wget -c https://s3.us-east-2.amazonaws.com/frostt/frostt_data/enron/enron.tns.gz
# wget -c https://s3.us-east-2.amazonaws.com/frostt/frostt_data/nell/nell-2.tns.gz
# wget -c https://s3.us-east-2.amazonaws.com/frostt/frostt_data/flickr/flickr-4d.tns.gz
# wget -c https://s3.us-east-2.amazonaws.com/frostt/frostt_data/flickr/flickr-3d.tns.gz
# wget -c https://s3.us-east-2.amazonaws.com/frostt/frostt_data/delicious/delicious-4d.tns.gz
# wget -c https://s3.us-east-2.amazonaws.com/frostt/frostt_data/delicious/delicious-3d.tns.gz
# wget -c https://s3.us-east-2.amazonaws.com/frostt/frostt_data/nell/nell-1.tns.gz
# wget -c https://suitesparse-collection-website.herokuapp.com/MM/SNAP/com-Orkut.tar.gz
# wget -c https://suitesparse-collection-website.herokuapp.com/MM/Janna/Queen_4147.tar.gz
# wget -c https://suitesparse-collection-website.herokuapp.com/MM/GenBank/kmer_A2a.tar.gz
# 
# 
# 
# for file in $FILES
# do
#     gzip -f -d $file".tns.gz"
# done
# 
# for file in $FILES
# do
#    ../convertToBin -f $file".tns"  
# done



for file in $FILES
do
    ../exps_hedgeQueries -f $file".tns.bin" -q 10000000 -r 5 -b -t 0.5 > ../outputs_data/$file".out"
    echo "$file done."
done




#for matrices

MTXFILES="
com-Orkut
Queen_4147
kmer_A2a
"

for mtxfile in $MTXFILES
do
    tar -xf $mtxfile".tar.gz"
    mv "$mtxfile/$mtxfile.mtx" .
done

for mtxfile in $MTXFILES
do
   ../convertToBin -f $mtxfile".mtx" -m 
done

for mtxfile in $MTXFILES
do
    ../exps_hedgeQueries -f $mtxfile".mtx.bin" -q 10000000 -r 5 -b -t 0.5 > ../outputs_data/$mtxfile".out"
    echo "$mtxfile done."
done


./table_2_3.sh

./figure_4_5_6.sh

./figure_7.sh

rm -f ../outputs_data/*.out
